<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//creacion de tabla users

		Schema::create('usuarios', function($tabla) {
			// id auto incremental (Clave Primaria)
			 $tabla->increments('id');

			 // varchar 32
			 $tabla->string('usuario', 32);
			 $tabla->string('email', 320);
			 $tabla->string('password', 64);

			 // int
			 $tabla->integer('rol');

			// created_at | updated_at DATETIME
			 $tabla->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//eliminacion de la tabla users

		Schema::drop('usuarios');
	}

}
