<?php
class Usuarios
{

	public static function insert_users($usuario,$email,$password)
	{

		$query = DB::table('usuarios')->insert(array(
				 'usuario' => $usuario,
				 'email'  => $email,
				 'password' => Crypter::encrypt($password)
		));

		return $query;

	}	

}