
<!doctype html>
<html>
<head>
	@include('includes.head')
</head>
<body>
<div class="">

	<header class="row">
		@include('includes.header')
	</header>

<!--	<div id="main" class="row">   -->
	<div class="row"> 
			@yield('content')

	</div>

	<footer class="row">
		@include('includes.footer')
	</footer>

</div>
</body>
</html>
