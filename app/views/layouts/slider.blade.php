
    <!-- begin:slider -->
    <div id="home">
      <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#home-slider" data-slide-to="0" class="active"></li>
          <li data-target="#home-slider" data-slide-to="1"></li>
          <li data-target="#home-slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item active" style="background: url(img/banner1.png);">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="carousel-content">

                    <h2 class="animated fadeInUpBig text-center text-black"><img src="img/logo.png" alt="dodolan manuk responsive catalog themes">

                    </h2>
                      <p class="animated rollIn text-black text-center"><span class="text900">BIENVENIDO A LA PÁGINA WEB DE GL TRACKER</span> <!-- an awesome catalog theme, <br> built with <i class="fa fa-heart-o"></i> in <span class="text900">Ngayogyokarto hadiningrat. --></span><br><br> <!-- <a href="#" class="btn btn-black btn-lg">Get Started</a> --></p>  
                  </div>
                </div> 
              </div>
            </div>
          </div>
          <div class="item" style="background: url(img/banner2.png);">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="carousel-content">
                  	<h3 class="animated fadeInLeftBig text-left">CONTACTENOS</h3>
					<p class="animated fadeInDownBig text-left"> Usted puede obtener nuestros servicios de rastreo satelital<br> para vehiculos de carga, trasporte y celulares <br>   contactándonos con nosotros</p>
					<!-- <a class="btn btn-black btn-lg animated fadeInRight" href="#">Learn more &raquo;</a> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#home-slider" data-slide="prev">
          <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#home-slider" data-slide="next">
          <i class="fa fa-angle-right"></i>
        </a>
        <div class="pattern"></div>
      </div>
    </div>
    <!-- end:slider -->