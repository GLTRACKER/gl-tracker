<!DOCTYPE html>
<html lang="es">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
	    <meta name="description" content="dodolan manuk responsive catalog themes built with twitter bootstrap">
	    <meta name="keywords" content="responsive, catalog, cart, themes, twitter bootstrap, bootstrap">
	    <meta name="author" content="afriq yasin ramadhan">
	    <link rel="shortcut icon" href="img/logo.gif">

	    <title>GL TRACKER</title>

	    <!-- Bootstrap core CSS -->
	    <link href="css/bootstrap.css" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="css/style.css" rel="stylesheet">
	    <link href="css/responsive.css" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="js/html5shiv.js"></script>
	      <script src="js/respond.min.js"></script>
	    <![endif]-->
	</head>

	<body>
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	    	<div class="container">
		        <!-- Brand and toggle get grouped for better mobile display -->
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand visible-xs" href="#">GL TRACKER</a>
		        </div>

		        <!-- Collect the nav links, forms, and other content for toggling -->
		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		        	<ul class="nav navbar-nav nav-left">
					  <li class="active"><a href="index.html">Inicio</a></li>
					  <li><a href="#">Nosotros</a></li>
					  <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Productos<b class="caret"></b></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Vehiculos</a></li>
						  <li><a href="#">Moviles</a></li>
						  <li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Doro <i class="fa fa-caret-down icon-dropdown"></i></a>
							<ul class="dropdown-menu sub-menu">
							  <li><a href="product.html">Android</a></li>
							  <li><a href="product.html">Blackbery</a></li>
							  <li><a href="product.html">SO IS</a></li>				
							</ul>
						  </li>
						  <li class="divider"></li>
						  <li><a href="product.html">Pitik</a></li>
						  <li><a href="product.html">Cucak Rowo</a></li>
						</ul>
					  </li>			  
					</ul>
					<a href="#" class="logo visible-lg visible-md"><img src="img/logo.gif" alt="dodolan manuk responsive catalog themes"></a>
					<div id="brand" class="visible-lg visible-md">&nbsp;</div>
					<ul class="nav navbar-nav nav-right">
						<li><a href="#">Distribuidores</a></li>
						<li><a href="#">Apoyo</a></li>
						<li><a href="#">Contáctanos</a></li>			  
					</ul>		          
		        </div><!-- /.navbar-collapse -->
		    </div><!-- /.container-fluid -->
	    </nav>



    <!-- begin:tagline -->	
	<div id="tagline">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>BIENVENIDO A LA PAGINA WEB DE GL TRACKER</h2>
					<p>
					Nos es grato su visita en nuestra website. Estamos a su disposición para brindarles todos los productos y servicios que ofrecemos y asesorarlo para el desarrollo e implantación de las nuevas tecnologías con GPS en su Empresa.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end:tagline -->
