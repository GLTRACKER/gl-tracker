		<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	    	<div class="container">
		        <!-- Brand and toggle get grouped for better mobile display -->
		        <div class="navbar-header">
		          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <a class="navbar-brand visible-xs" href="#">GL TRACKER</a>
		        </div>

		        <!-- Collect the nav links, forms, and other content for toggling -->
		        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		        	<ul class="nav navbar-nav nav-left">
					  <li class="active"><a href="index.html">Inicio</a></li>
					  <li class="dropdown">
					  	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Nosotros<b class="caret"></b></a>
					  	<ul class="dropdown-menu">
						  <li><a href="#">Historia</a></li>					
						  <li><a href="product.html">Misio y visión</a></li>						  
						</ul>
					  </li>
					  <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Productos<b class="caret"></b></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Vehiculos</a></li>
						  <li><a href="#">Moviles</a></li>
						  <li>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Doro <i class="fa fa-caret-down icon-dropdown"></i></a>
							<ul class="dropdown-menu sub-menu">
							  <li><a href="product.html">Android</a></li>
							  <li><a href="product.html">Blackbery</a></li>
							  <li><a href="product.html">SO IS</a></li>				
							</ul>
						  </li>
						  <li class="divider"></li>
						  <li><a href="product.html">Pitik</a></li>
						  <li><a href="product.html">Cucak Rowo</a></li>
						</ul>
					  </li>			  
					</ul>
					<a href="#" class="logo visible-lg visible-md"><img src="img/logo.gif" alt="dodolan manuk responsive catalog themes"></a>
					<div id="brand" class="visible-lg visible-md">&nbsp;</div>
					<ul class="nav navbar-nav nav-right">
						<li ><a href="#">Distribuidores</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Apoyo<b class="caret"></b></a>
							<ul class="dropdown-menu">
							  <li><a href="#">Preguntas frecuentes</a></li>
							</ul>
						</li>
						<li><a href="#">Contáctanos</a></li>			  
					</ul>		          
		        </div><!-- /.navbar-collapse -->
		    </div><!-- /.container-fluid -->
	    </nav>