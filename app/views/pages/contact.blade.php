@extends('layouts.default')
@section('content')

	
	<div class="heads" >
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2 ><span  class="glyphicon glyphicon-envelope"> CONTACTENOS</span> </h2>
				</div>
			</div>
		</div>
	</div>
	<!-- end:heading -->

	<!-- begin:contact -->
	<div class="page-content contact">
		<div class="container">
						
			<div class="row">
				<div class="col-md-12 text-center">
				<!--	<h3>Lorem ipsum dolor sit amet</h3> -->
					<h3><p>Para consultas, duda y otras interrogantes sirvase a enviarnos un mensaje</p></h3>
				</div>
			</div>

			
			<div class="row padd20-top-btm">

			{{ Form::open(array('url' => '/login', 'method' => 'POST')) }}
				<form method="post" action="contactenos">
					<div class="col-md-5 col-sm-5">
						<h3>ENVIAR MENSAJES</h3>
						<input type="text" name="nobre" class="form-control" placeholder="Ingrese su nombre" required>
						<input type="email" name="correo" class="form-control" placeholder="Ingrese su correo" required>
						<input type="text" name="tema" class="form-control" placeholder="Ingrese el tema" required>
					</div>
					<div class="col-md-7 col-sm-7">
						<textarea name="mensaje" class="form-control" rows="7" placeholder="Escribe el mensaje" required></textarea>
						<input type="submit" name="submit" value="enviar mensaje" class="btn btn-black btn-block btn-lg">
					</div>			
			{{ Form::close() }}

			</div>
		</div>
	</div>
	
	<div id="maps"></div>
	<!-- end:contact -->



	<!-- begin:footer -->
	<div id="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>OTROS</h3>
			<p>Para mis informacion revise nuestras redes sociales</p>

					<ul class="list-unstyled social-icon">
		              <li><a href="https://www.facebook.com/gl.tracker.5/" rel="tooltip" title="Facebook" class="icon-facebook"><span><i class="fa fa-facebook-square"></i></span></a></li>
		              <li><a href="#" rel="tooltip" title="Twitter" class="icon-twitter"><span><i class="fa fa-twitter"></i></span></a></li>
		            <!--  <li><a href="#" rel="tooltip" title="Linkedin" class="icon-linkedin"><span><i class="fa fa-linkedin"></i></span></a></li>-->
		              <li><a href="#" rel="tooltip" title="Instagram" class="icon-gplus"><span><i class="fa fa-google-plus"></i></span></a></li>
		            <!--  <li><a href="#" rel="tooltip" title="Instagram" class="icon-instagram"><span><i class="fa fa-instagram"></i></span></a></li> -->
		            </ul>


				</div>
			</div>
		</div>
	</div>
	<!-- end:footer -->
	


@stop