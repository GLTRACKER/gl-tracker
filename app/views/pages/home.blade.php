@extends('layouts.default')
@section('content')

@include('layouts.slider')
    
    <!-- begin:tagline -->	
	<div id="tagline">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>BIENVENIDO A LA PAGINA WEB DE GL TRACKER</h2>
					<p>
					Nos es grato su visita en nuestra website. Estamos a su disposición para brindarles todos los productos y servicios que ofrecemos y asesorarlo para el desarrollo e implantación de las nuevas tecnologías con GPS en su Empresa.</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end:tagline -->


@stop